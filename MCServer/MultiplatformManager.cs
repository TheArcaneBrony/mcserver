﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace MCServer
{
    class MultiplatformManager
    {
        public static bool IsDocker = false;
        //if docker, use /data
        public static string GetBasePath() => IsDocker? "/data" : Environment.CurrentDirectory + "/data";
        public static string GetDataPath()
        {
            return GetDataPath(GetBasePath());
        }
        public static string GetDataPath(string dir)
        {
            List<string> dpp = new List<string>();
            foreach (string p in dir.Split('/')) dpp.AddRange(p.Split('\\'));
            string pp = "";
            Console.Write(String.Join('/', dpp) + "\r");
            ConsoleColor oc = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            //Thread.Sleep(100);
            bool allexist = true;
            foreach (string p in dpp)
            {
                pp += p + "/";
                if (Directory.Exists(pp))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write($"{p}/");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{p}/");
                    Directory.CreateDirectory(pp);
                    allexist = false;
                }
                //Thread.Sleep(50);
            }
            Console.ForegroundColor = oc;
            /*if (!allexist) Console.WriteLine("\nCreated non existing tree");
            else Console.WriteLine("\nDid not need to create folders");*/
            Console.WriteLine();
            return dir;
        }
        public static string GetDataFolderPath(string path) => GetDataPath(GetBasePath()+"/"+path);
        public static string GetDataFilePath(string path) {
            List<string> dpp = new List<string>();
            foreach (string p in (GetBasePath()+"/"+path).Split('/')) dpp.AddRange(p.Split('\\'));
            string file = dpp.Last();
            dpp.Remove(dpp.Last());
            string dpath = String.Join('/', dpp);

            return GetDataPath(dpath) + "/" + file;
        }

    }
}
