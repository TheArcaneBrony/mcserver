﻿using MCServer.GameData;

using System;
using System.Linq;
using System.Text;
using java.net;
using MCServer.Network;
using Socket = System.Net.Sockets.Socket;

namespace MCServer
{
    class Program
    {
        static VersionData VersionData;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.Write("Checking for docker... ");
            if (args.Contains("--docker"))
            {
                MultiplatformManager.IsDocker = true;
            }
            Console.WriteLine($"{MultiplatformManager.IsDocker}.");
            Console.WriteLine($"Checking if path is valid:");
            MultiplatformManager.GetDataPath();
            //MultiplatformManager.GetDataFolderPath("versions");
            MultiplatformManager.GetDataFolderPath("world/dim-1");
            MultiplatformManager.GetDataFolderPath("world/dim0");
            MultiplatformManager.GetDataFolderPath("world/dim1");
            //MultiplatformManager.GetDataFolderPath("");
            VersionData versionData_1152 = VersionData.GetVersionData("1.15.2");
            VersionData.GetVersionDataRewrite("1.15.2");
            NetworkListener.StartListening();
            //NetworkListener.DumpServerListQuery();

            Console.ReadLine();
        }
    }
}
