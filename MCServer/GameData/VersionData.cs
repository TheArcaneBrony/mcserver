﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.IO;

namespace MCServer.GameData
{
    class VersionData
    {
        public static VersionData GetVersionData(string v)
        {
            Console.WriteLine($"{DateTime.Now.TimeOfDay} Getting version data for: {v}");
            VersionData versionData = new VersionData();
            Console.WriteLine($"{DateTime.Now.TimeOfDay} Loading block data");
            versionData._Blocks = JsonConvert.DeserializeObject<Dictionary<string, _Block>>(File.ReadAllText(MultiplatformManager.GetDataFilePath($"versions/{v}/reports/blocks.json")));
            foreach (KeyValuePair<string, _Block> bl in versionData._Blocks) versionData.Blocks.Add(bl.Key, bl.Value.GetBlock());
            Console.WriteLine(DateTime.Now.TimeOfDay + " Finished loading block data");
            return versionData;
        }
        public static NewVersionData GetVersionDataRewrite(string v)
        {
            Console.WriteLine($"{DateTime.Now.TimeOfDay} Getting version data for: {v} (rewrite)");
            NewVersionData versionData = new();
            Console.WriteLine($"{DateTime.Now.TimeOfDay} Loading block data");
            versionData.Blocks = JsonConvert.DeserializeObject<Dictionary<string, NewBlock>>(File.ReadAllText(MultiplatformManager.GetDataFilePath($"versions/{v}/reports/blocks.json")));
            //foreach (KeyValuePair<string, _Block> bl in versionData._Blocks) versionData.Blocks.Add(bl.Key, bl.Value.GetBlock());
            Console.WriteLine(DateTime.Now.TimeOfDay + " Finished loading block data");
            return versionData;
        }
        public VersionData() { }
        public static void CheckDataStructure(string version)
        {

        }
        Dictionary<string, Block> Blocks = new Dictionary<string, Block>();
        Dictionary<string, _Block> _Blocks;


        private class _Block
        {
            public Block GetBlock()
            {
                Block bl = new Block();
                List<BlockState> bls = new List<BlockState>();
                //if(/*_BlockStates != null || */_BlockProperties != null) Console.WriteLine($"Registered block with {(_BlockStates == null ? 0 : _BlockStates.Length)} blockstates and {(_BlockProperties == null ? 0 : _BlockProperties.Count)} properties.");
                foreach (_BlockState b in _BlockStates) bl.BlockStates.Add(b.GetBlockState());
                bl.BlockStates = bls;
                if (_BlockProperties != null)
                {
                    BlockProperty fblp = new BlockProperty();
                    foreach (KeyValuePair<string, object> blp in _BlockProperties)
                    {
                        try
                        {

                        }
                        catch (ArgumentNullException e)
                        {
                            Console.WriteLine($"Failed to block property {blp.Key} with value(s) {string.Join(", ", blp.Value)}:\n{e.Message}\n{e.StackTrace}");
                        }
                        //Console.WriteLine($"Registering block property {blp.Key} with value(s) {string.Join(", ", blp.Value)}");
                        if (blp.Key == "snowy")
                        {
                            if (fblp.Snowy == null) fblp.Snowy = new List<bool>();
                            fblp.Snowy.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "stage")
                        {
                            if (fblp.Stage == null) fblp.Stage = new List<int>();
                            fblp.Stage.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "level")
                        {
                            if (fblp.Level == null) fblp.Level = new List<int>();
                            fblp.Level.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "age")
                        {
                            if (fblp.Age == null) fblp.Age = new List<int>();
                            fblp.Age.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "honey_level")
                        {
                            if (fblp.HoneyLevel == null) fblp.HoneyLevel = new List<int>();
                            fblp.HoneyLevel.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "axis")
                        {
                            if (fblp.Axis == null) fblp.Axis = new List<string>();
                            fblp.Axis.Add(blp.Value as string);
                        }
                        else if (blp.Key == "attachment")
                        {
                            if (fblp.Attachment == null) fblp.Attachment = new List<string>();
                            fblp.Attachment.Add(blp.Value as string);
                        }
                        else if (blp.Key == "distance")
                        {
                            if (fblp.Distance == null) fblp.Distance = new List<int>();
                            fblp.Distance.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "persistent")
                        {
                            if (fblp.Persistent == null) fblp.Persistent = new List<bool>();
                            fblp.Persistent.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "waterlogged")
                        {
                            if (fblp.Waterlogged == null) fblp.Waterlogged = new List<bool>();
                            fblp.Waterlogged.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "signal_fire")
                        {
                            if (fblp.Signal_Fire == null) fblp.Signal_Fire = new List<bool>();
                            fblp.Signal_Fire.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "lit")
                        {
                            if (fblp.Lit == null) fblp.Lit = new List<bool>();
                            fblp.Lit.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "powered")
                        {
                            if (fblp.Powered == null) fblp.Powered = new List<bool>();
                            fblp.Powered.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "power")
                        {
                            if (fblp.Power == null) fblp.Power = new List<int>();
                            fblp.Power.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "up")
                        {
                            if (fblp.Up == null) fblp.Up = new List<bool>();
                            fblp.Up.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "down")
                        {
                            if (fblp.Down == null) fblp.Down = new List<bool>();
                            fblp.Down.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "bottom")
                        {
                            if (fblp.Bottom == null) fblp.Bottom = new List<bool>();
                            fblp.Bottom.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "north")
                        {
                            if (fblp.North == null) fblp.North = new List<bool>();
                            fblp.North.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "east")
                        {
                            if (fblp.East == null) fblp.East = new List<bool>();
                            fblp.East.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "south")
                        {
                            if (fblp.South == null) fblp.South = new List<bool>();
                            fblp.South.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "west")
                        {
                            if (fblp.West == null) fblp.West = new List<bool>();
                            fblp.West.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "facing")
                        {
                            if (fblp.Facing == null) fblp.Facing = new List<string>();
                            fblp.Facing.Add(blp.Value as string);
                        }

                        else if (blp.Key == "type")
                        {
                            if (fblp.Type == null) fblp.Type = new List<string>();
                            fblp.Type.Add(blp.Value as string);
                        }
                        else if (blp.Key == "shape")
                        {
                            if (fblp.Shape == null) fblp.Shape = new List<string>();
                            fblp.Shape.Add(blp.Value as string);
                        }
                        else if (blp.Key == "half")
                        {
                            if (fblp.Half == null) fblp.Half = new List<string>();
                            fblp.Half.Add(blp.Value as string);
                        }
                        else if (blp.Key == "hinge")
                        {
                            if (fblp.Hinge == null) fblp.Hinge = new List<string>();
                            fblp.Hinge.Add(blp.Value as string);
                        }
                        else if (blp.Key == "open")
                        {
                            if (fblp.Open == null) fblp.Open = new List<bool>();
                            fblp.Open.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "attached")
                        {
                            if (fblp.Attached == null) fblp.Attached = new List<bool>();
                            fblp.Attached.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "disarmed")
                        {
                            if (fblp.Disarmed == null) fblp.Disarmed = new List<bool>();
                            fblp.Disarmed.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "occupied")
                        {
                            if (fblp.Occupied == null) fblp.Occupied = new List<bool>();
                            fblp.Occupied.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "part")
                        {
                            if (fblp.Part == null) fblp.Part = new List<string>();
                            fblp.Part.Add((blp.Value as string));
                        }
                        else if (blp.Key == "conditional")
                        {
                            if (fblp.Conditional == null) fblp.Conditional = new List<bool>();
                            fblp.Conditional.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "note")
                        {
                            if (fblp.Note == null) fblp.Note = new List<int>();
                            fblp.Note.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "instrument")
                        {
                            if (fblp.Instrument == null) fblp.Instrument = new List<string>();
                            fblp.Instrument.Add((blp.Value as string));
                        }
                        else if (blp.Key == "extended")
                        {
                            if (fblp.Extended == null) fblp.Extended = new List<bool>();
                            fblp.Extended.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "short")
                        {
                            if (fblp.Short == null) fblp.Short = new List<bool>();
                            fblp.Short.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "triggered")
                        {
                            if (fblp.Triggered == null) fblp.Triggered = new List<bool>();
                            fblp.Triggered.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "inverted")
                        {
                            if (fblp.Inverted == null) fblp.Inverted = new List<bool>();
                            fblp.Inverted.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "locked")
                        {
                            if (fblp.Locked == null) fblp.Locked = new List<bool>();
                            fblp.Locked.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "has_bottle_0")
                        {
                            if (fblp.HasBottle0 == null) fblp.HasBottle0 = new List<bool>();
                            fblp.HasBottle0.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "has_bottle_1")
                        {
                            if (fblp.HasBottle1 == null) fblp.HasBottle1 = new List<bool>();
                            fblp.HasBottle1.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "has_bottle_2")
                        {
                            if (fblp.HasBottle2 == null) fblp.HasBottle2 = new List<bool>();
                            fblp.HasBottle2.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "delay")
                        {
                            if (fblp.Delay == null) fblp.Delay = new List<int>();
                            fblp.Delay.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "has_book")
                        {
                            if (fblp.HasBook == null) fblp.HasBook = new List<bool>();
                            fblp.HasBook.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "enabled")
                        {
                            if (fblp.Enabled == null) fblp.Enabled = new List<bool>();
                            fblp.Enabled.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "drag")
                        {
                            if (fblp.Drag == null) fblp.Drag = new List<bool>();
                            fblp.Drag.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "hanging")
                        {
                            if (fblp.Hanging == null) fblp.Hanging = new List<bool>();
                            fblp.Hanging.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "leaves")
                        {
                            if (fblp.Leaves == null) fblp.Leaves = new List<string>();
                            fblp.Leaves.Add((blp.Value as string));
                        }
                        else if (blp.Key == "mode")
                        {
                            if (fblp.Mode == null) fblp.Mode = new List<string>();
                            fblp.Mode.Add((blp.Value as string));
                        }
                        else if (blp.Key == "moisture")
                        {
                            if (fblp.Moisture == null) fblp.Moisture = new List<int>();
                            fblp.Moisture.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "layers")
                        {
                            if (fblp.Layers == null) fblp.Layers = new List<int>();
                            fblp.Layers.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "bites")
                        {
                            if (fblp.Bites == null) fblp.Bites = new List<int>();
                            fblp.Bites.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "eggs")
                        {
                            if (fblp.Eggs == null) fblp.Eggs = new List<int>();
                            fblp.Eggs.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "hatch")
                        {
                            if (fblp.Hatch == null) fblp.Hatch = new List<int>();
                            fblp.Hatch.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "pickles")
                        {
                            if (fblp.Pickles == null) fblp.Pickles = new List<int>();
                            fblp.Pickles.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else if (blp.Key == "eye")
                        {
                            if (fblp.Eye == null) fblp.Eye = new List<bool>();
                            fblp.Eye.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "unstable")
                        {
                            if (fblp.Unstable == null) fblp.Unstable = new List<bool>();
                            fblp.Unstable.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "has_record")
                        {
                            if (fblp.HasRecord == null) fblp.HasRecord = new List<bool>();
                            fblp.HasRecord.Add((blp.Value as string) == "true");
                        }


                        else if (blp.Key == "face")
                        {
                            if (fblp.Face == null) fblp.Face = new List<string>();
                            fblp.Face.Add((blp.Value as string));
                        }
                        else if (blp.Key == "in_wall")
                        {
                            if (fblp.InWall == null) fblp.InWall = new List<bool>();
                            fblp.InWall.Add((blp.Value as string) == "true");
                        }
                        else if (blp.Key == "rotation")
                        {
                            if (fblp.Rotation == null) fblp.Rotation = new List<int>();
                            fblp.Rotation.Add(int.Parse(blp.Value as string ?? "0"));
                        }
                        else
                        {
                            Console.WriteLine($"Unrecognised block property {blp.Key} with value(s) {string.Join(", ", blp.Value)}");
                        }
                    }
                    bl.BlockProperties = fblp;
                }

                return bl;
            }
            [JsonProperty("states")]
            _BlockState[] _BlockStates;
            [JsonProperty("properties")]
            Dictionary<string, object> _BlockProperties;



            private class _BlockState
            {
                public _BlockState() { }
                public BlockState GetBlockState()
                {
                    BlockState bls = new BlockState();
                    bls.ID = ID;
                    bls.Default = Default;
                    //Console.WriteLine("Registering new block state with ID: " + bls.ID);
                    if (BlockProperties != null)
                    {
                        BlockProperty fblp = new BlockProperty();
                        foreach (KeyValuePair<string, object> blp in BlockProperties)
                        {
                            //Console.WriteLine($"Registering block property {blp.Key} with value(s) {string.Join(", ", blp.Value)}");
                            if (blp.Key == "snowy")
                            {
                                if (fblp.Snowy == null) fblp.Snowy = new List<bool>();
                                fblp.Snowy.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "stage")
                            {
                                if (fblp.Stage == null) fblp.Stage = new List<int>();
                                fblp.Stage.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "level")
                            {
                                if (fblp.Level == null) fblp.Level = new List<int>();
                                fblp.Level.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "age")
                            {
                                if (fblp.Age == null) fblp.Age = new List<int>();
                                fblp.Age.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "honey_level")
                            {
                                if (fblp.HoneyLevel == null) fblp.HoneyLevel= new List<int>();
                                fblp.HoneyLevel.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "axis")
                            {
                                if (fblp.Axis == null) fblp.Axis = new List<string>();
                                fblp.Axis.Add(blp.Value as string);
                            }
                            else if (blp.Key == "attachment")
                            {
                                if (fblp.Attachment == null) fblp.Attachment = new List<string>();
                                fblp.Attachment.Add(blp.Value as string);
                            }
                            else if (blp.Key == "distance")
                            {
                                if (fblp.Distance == null) fblp.Distance = new List<int>();
                                fblp.Distance.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "persistent")
                            {
                                if (fblp.Persistent == null) fblp.Persistent = new List<bool>();
                                fblp.Persistent.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "waterlogged")
                            {
                                if (fblp.Waterlogged == null) fblp.Waterlogged = new List<bool>();
                                fblp.Waterlogged.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "signal_fire")
                            {
                                if (fblp.Signal_Fire == null) fblp.Signal_Fire = new List<bool>();
                                fblp.Signal_Fire.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "lit")
                            {
                                if (fblp.Lit == null) fblp.Lit = new List<bool>();
                                fblp.Lit.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "powered")
                            {
                                if (fblp.Powered == null) fblp.Powered = new List<bool>();
                                fblp.Powered.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "power")
                            {
                                if (fblp.Power == null) fblp.Power = new List<int>();
                                fblp.Power.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "up")
                            {
                                if (fblp.Up == null) fblp.Up = new List<bool>();
                                fblp.Up.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "down")
                            {
                                if (fblp.Down == null) fblp.Down = new List<bool>();
                                fblp.Down.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "bottom")
                            {
                                if (fblp.Bottom == null) fblp.Bottom = new List<bool>();
                                fblp.Bottom.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "north")
                            {
                                if (fblp.North == null) fblp.North = new List<bool>();
                                fblp.North.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "east")
                            {
                                if (fblp.East == null) fblp.East = new List<bool>();
                                fblp.East.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "south")
                            {
                                if (fblp.South == null) fblp.South = new List<bool>();
                                fblp.South.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "west")
                            {
                                if (fblp.West == null) fblp.West = new List<bool>();
                                fblp.West.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "facing")
                            {
                                if (fblp.Facing== null) fblp.Facing= new List<string>();
                                fblp.Facing.Add(blp.Value as string);
                            }

                            else if (blp.Key == "type")
                            {
                                if (fblp.Type == null) fblp.Type = new List<string>();
                                fblp.Type.Add(blp.Value as string);
                            }
                            else if (blp.Key == "shape")
                            {
                                if (fblp.Shape == null) fblp.Shape = new List<string>();
                                fblp.Shape.Add(blp.Value as string);
                            }
                            else if (blp.Key == "half")
                            {
                                if (fblp.Half == null) fblp.Half = new List<string>();
                                fblp.Half.Add(blp.Value as string);
                            }
                            else if (blp.Key == "hinge")
                            {
                                if (fblp.Hinge == null) fblp.Hinge = new List<string>();
                                fblp.Hinge.Add(blp.Value as string);
                            }
                            else if (blp.Key == "open")
                            {
                                if (fblp.Open == null) fblp.Open = new List<bool>();
                                fblp.Open.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "attached")
                            {
                                if (fblp.Attached == null) fblp.Attached = new List<bool>();
                                fblp.Attached.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "disarmed")
                            {
                                if (fblp.Disarmed == null) fblp.Disarmed = new List<bool>();
                                fblp.Disarmed.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "occupied")
                            {
                                if (fblp.Occupied == null) fblp.Occupied = new List<bool>();
                                fblp.Occupied.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "part")
                            {
                                if (fblp.Part == null) fblp.Part = new List<string>();
                                fblp.Part.Add((blp.Value as string));
                            }
                            else if (blp.Key == "conditional")
                            {
                                if (fblp.Conditional == null) fblp.Conditional = new List<bool>();
                                fblp.Conditional.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "note")
                            {
                                if (fblp.Note == null) fblp.Note = new List<int>();
                                fblp.Note.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "instrument")
                            {
                                if (fblp.Instrument == null) fblp.Instrument = new List<string>();
                                fblp.Instrument.Add((blp.Value as string));
                            }
                            else if (blp.Key == "extended")
                            {
                                if (fblp.Extended == null) fblp.Extended = new List<bool>();
                                fblp.Extended.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "short")
                            {
                                if (fblp.Short == null) fblp.Short = new List<bool>();
                                fblp.Short.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "triggered")
                            {
                                if (fblp.Triggered == null) fblp.Triggered = new List<bool>();
                                fblp.Triggered.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "inverted")
                            {
                                if (fblp.Inverted == null) fblp.Inverted = new List<bool>();
                                fblp.Inverted.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "locked")
                            {
                                if (fblp.Locked == null) fblp.Locked = new List<bool>();
                                fblp.Locked.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "has_bottle_0")
                            {
                                if (fblp.HasBottle0 == null) fblp.HasBottle0 = new List<bool>();
                                fblp.HasBottle0.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "has_bottle_1")
                            {
                                if (fblp.HasBottle1 == null) fblp.HasBottle1 = new List<bool>();
                                fblp.HasBottle1.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "has_bottle_2")
                            {
                                if (fblp.HasBottle2 == null) fblp.HasBottle2 = new List<bool>();
                                fblp.HasBottle2.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "delay")
                            {
                                if (fblp.Delay == null) fblp.Delay = new List<int>();
                                fblp.Delay.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "has_book")
                            {
                                if (fblp.HasBook == null) fblp.HasBook = new List<bool>();
                                fblp.HasBook.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "enabled")
                            {
                                if (fblp.Enabled == null) fblp.Enabled = new List<bool>();
                                fblp.Enabled.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "drag")
                            {
                                if (fblp.Drag == null) fblp.Drag = new List<bool>();
                                fblp.Drag.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "hanging")
                            {
                                if (fblp.Hanging == null) fblp.Hanging = new List<bool>();
                                fblp.Hanging.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "leaves")
                            {
                                if (fblp.Leaves == null) fblp.Leaves = new List<string>();
                                fblp.Leaves.Add((blp.Value as string));
                            }
                            else if (blp.Key == "mode")
                            {
                                if (fblp.Mode == null) fblp.Mode = new List<string>();
                                fblp.Mode.Add((blp.Value as string));
                            }
                            else if (blp.Key == "moisture")
                            {
                                if (fblp.Moisture == null) fblp.Moisture = new List<int>();
                                fblp.Moisture.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "layers")
                            {
                                if (fblp.Layers == null) fblp.Layers = new List<int>();
                                fblp.Layers.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "bites")
                            {
                                if (fblp.Bites == null) fblp.Bites = new List<int>();
                                fblp.Bites.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "eggs")
                            {
                                if (fblp.Eggs == null) fblp.Eggs = new List<int>();
                                fblp.Eggs.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "hatch")
                            {
                                if (fblp.Hatch == null) fblp.Hatch = new List<int>();
                                fblp.Hatch.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "pickles")
                            {
                                if (fblp.Pickles == null) fblp.Pickles = new List<int>();
                                fblp.Pickles.Add(int.Parse(blp.Value as string));
                            }
                            else if (blp.Key == "eye")
                            {
                                if (fblp.Eye == null) fblp.Eye = new List<bool>();
                                fblp.Eye.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "unstable")
                            {
                                if (fblp.Unstable == null) fblp.Unstable = new List<bool>();
                                fblp.Unstable.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "has_record")
                            {
                                if (fblp.HasRecord == null) fblp.HasRecord = new List<bool>();
                                fblp.HasRecord.Add((blp.Value as string) == "true");
                            }


                            else if (blp.Key == "face")
                            {
                                if (fblp.Face == null) fblp.Face = new List<string>();
                                fblp.Face.Add((blp.Value as string));
                            }
                            else if (blp.Key == "in_wall")
                            {
                                if (fblp.InWall == null) fblp.InWall = new List<bool>();
                                fblp.InWall.Add((blp.Value as string) == "true");
                            }
                            else if (blp.Key == "rotation")
                            {
                                if (fblp.Rotation == null) fblp.Rotation = new List<int>();
                                fblp.Rotation.Add(int.Parse(blp.Value as string));
                            }
                            else
                            {
                                Console.WriteLine($"Unrecognised block property {blp.Key} with value(s) {string.Join(", ", blp.Value)}");
                            }
                            bls.BlockProperties = fblp;
                        }
                    }

                    return bls;


                }
                [JsonProperty("id")]
                public int ID;
                [JsonProperty("default")]
                public bool Default;
                [JsonProperty("properties")]
                Dictionary<string, object> BlockProperties;
            }
        }
    }


    class Block
    {
        [JsonProperty("states")]
        public List<BlockState> BlockStates = new List<BlockState>();
        [JsonProperty("properties")]
        public BlockProperty BlockProperties;
    }
    class BlockState
    {
        public BlockState() { }
        public int ID;
        public bool Default;
        //Dictionary<string, string[]> BlockProperties;
        public BlockProperty BlockProperties;
    }
    class BlockProperty
    {
        public BlockProperty() { }
        public List<string> Axis;
        public List<string> Facing;
        public List<string> Attachment;
        public List<string> Hinge;
        public List<string> Face;
        public List<string> Part;
        public List<string> Leaves;
        public List<string> Mode;
        public List<int> Delay;
        public List<int> Moisture;
        public List<int> Layers;
        public List<int> Bites;
        public List<int> Eggs;
        public List<int> Hatch;
        public List<int> Pickles;
        public List<int> Distance;
        public List<bool> Snowy;
        public List<bool> Eye;
        public List<bool> HasRecord;
        public List<bool> Unstable;
        public List<bool> Persistent;
        public List<bool> Conditional;
        public List<bool> Open;
        public List<bool> Powered;
        public List<bool> Occupied;
        public List<int> Power;
        public List<bool> Lit;
        public List<bool> HasBook;
        public List<bool> Enabled;
        public List<bool> Drag;
        public List<bool> Hanging;
        public List<bool> Waterlogged;
        public List<bool> Signal_Fire;
        public List<bool> InWall;
        public List<bool> Up;
        public List<bool> Down;
        public List<bool> East;
        public List<bool> West;
        public List<bool> North;
        public List<bool> South;
        public List<bool> Bottom;
        public List<bool> Attached;
        public List<bool> Disarmed;
        public List<bool> Extended;
        public List<bool> Short;
        public List<bool> Triggered;
        public List<bool> Inverted;
        public List<bool> Locked;
        public List<bool> HasBottle0;
        public List<bool> HasBottle1;
        public List<bool> HasBottle2;
        public List<int> Stage;
        public List<int> Level;
        public List<int> HoneyLevel;
        public List<int> Note;
        public List<string> Instrument;
        public List<string> Type;
        public List<string> Shape;
        public List<string> Half;
        public List<int> Age;
        public List<int> Rotation;
    }
}
