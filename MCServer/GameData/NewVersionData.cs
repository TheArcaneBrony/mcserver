﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MCServer.GameData
{
    public class NewVersionData
    {
        public Dictionary<string, NewBlock> Blocks;
    }

    public class NewBlock
    {
        [JsonProperty("states")]
        public NewBlockState[] States;
        
        public Dictionary<string, object> properties;
    }

    public class NewBlockState
    {
        public int id;
        public bool @default;
        public Dictionary<string, object> properties;
    }

    public class NewBlockProperty
    {
        
    }
}