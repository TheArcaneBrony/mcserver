﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MCServer.Network
{
    public class NetworkListener
    {
        public static void StartListening()
        {
            Thread thread = new Thread(() =>
            {
                TcpListener listener = TcpListener.Create(25565);
                listener.Start();
                Console.WriteLine("Started listening on port 25565, waiting for client");
                TcpClient client = listener.AcceptTcpClient();
                
                Console.WriteLine("Client connected!");
                Stream stream = client.GetStream();
                int length = stream.ReadByte();
                int packetID = stream.ReadByte();
                if (packetID == 0x00)
                {
                    Console.WriteLine("server list ping? lol");
                    ServerListPingHandler.HandleServerListPing(stream);
                }

                if (packetID == 0x01)
                {
                    stream.CopyTo(stream);
                }
                else if (packetID == 0xFE)
                {
                    List<byte> data = new List<byte>();
                    data.AddRange(new byte[]{0xFF, 0xff, 0xff});//kick packet
                    data.AddRange(Encoding.BigEndianUnicode.GetBytes("§1\0"));
                    stream.Write(data.ToArray());
                }
                else
                {
                    Console.WriteLine($"Unknown packet received: 0x{packetID:X2}");
                    FileStream file = File.Create($"mc-packets-0x{packetID:X2}.bin");
                    client.GetStream().CopyTo(file);
                    file.Close();
                }
                //client.GetStream().CopyTo(file);
                //file.Write(incommingTraffic);
                //stream.Close();
                //client.Close();
                Console.WriteLine("Client disconnected!");
                listener.Stop();
                StartListening();
            });
            thread.Start();
        }
        public static void DumpServerListQuery()
        {
            Thread thread = new Thread(() =>
            {
                TcpListener listener = TcpListener.Create(420);
                listener.Start();
                Console.WriteLine("Started listening on port 25565, waiting for client");
                TcpClient client = listener.AcceptTcpClient();
                
                Console.WriteLine("Client connected!");
                Stream stream = client.GetStream();
                
                FileStream file = File.Create("mc-packets.bin");
                stream.CopyTo(file);
                stream.Close();
                client.Close(); 
                Console.WriteLine("Client disconnected!");
                file.Close();
                Process.Start(@"C:\Program Files\HxD\HxD.exe", Environment.CurrentDirectory + "\\mc-packets.bin");
            });
            thread.Start();
        }
    }
}