﻿using System.Buffers.Binary;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace MCServer.Network
{
    public static class NetworkReader
    {
        //see: https://wiki.vg/Protocol#Data_types
        public static byte ReadUInt8(this Stream stream)
        {
            return (byte) stream.ReadByte();
        }
        public static bool ReadBoolean(this Stream stream)
        {
            return new BinaryReader(stream).ReadBoolean();
        }
        public static sbyte ReadInt8(this Stream stream)
        {
            return new BinaryReader(stream).ReadSByte();
        }

        public static short ReadInt16(this Stream stream)
        {
            byte[] data = new byte[2];
            stream.Read(data, 0, 2);
            return BinaryPrimitives.ReadInt16BigEndian(data);
        }

        public static ushort ReadUInt16(this Stream stream)
        {
            byte[] data = new byte[2];
            stream.Read(data, 0, 2);
            return BinaryPrimitives.ReadUInt16BigEndian(data);   
        }
        public static int ReadInt32(this Stream stream)
        {
            byte[] data = new byte[5];
            stream.Read(data, 0, 4);
            return BinaryPrimitives.ReadInt32BigEndian(data);
        }

        public static uint ReadUInt32(this Stream stream)
        {
            byte[] data = new byte[4];
            stream.Read(data, 0, 5);
            return BinaryPrimitives.ReadUInt32BigEndian(data);   
        }
        public static int ReadVarInt(this Stream stream)
        {
            var val = 0;
            var size = 0;
            int readData;
            
            while (((readData = ReadUInt8(stream)) & 0x80) == 0x80)
            {
                val |= (readData & 0x7F) << (size++ * 7);
                if (size > 5)
                {
                    throw new IOException("VarInt too long");
                }
            }

            return val | ((readData & 0x7F) << (size * 0x7));
        }

        public static string ReadString(this Stream stream)
        {
            byte len = stream.ReadUInt8();
            byte[] data = new byte[len];
            stream.Read(data, 0, len);
            return Encoding.UTF8.GetString(data);
            //return new BinaryReader(stream).ReadString();
        }
    }
}