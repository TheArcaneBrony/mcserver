﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace MCServer.Network
{
    public class ServerListPingHandler
    {
        public static void HandleServerListPing(Stream stream)
        {
            stream.ReadInt16(); // skip 2 bytes
            string hostname = stream.ReadString();
            ushort port = stream.ReadUInt16();
            int nextState = stream.ReadVarInt();
            Console.WriteLine($"hostname: {hostname}\nport: {port}\nnext state: {nextState}");
            if (nextState == 1)
            {
                Console.WriteLine("Received server list ping!");
                byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new ServerListPingResponse()));
                stream.Write(BitConverter.GetBytes(data.Length));
                stream.Write(data);
                Console.WriteLine(JsonConvert.SerializeObject(new ServerListPingResponse()));
            }
            else if (nextState == 2)
            {
                Console.WriteLine("Received player join!");
            }
            else
            {
                Console.WriteLine($"Received invalid server list state: {nextState}");
            }
            
        }

        private class ServerListPingResponse
        {
            public VersionData version = new VersionData();
            public PlayerListData players = new();
            public ChatObject description = new ChatObject();
        }
        private class VersionData
        {
            public string name = "1.15.2";
            public int protocol = 123;
        }
        private class PlayerListData
        {
            public int max = 100;
            public int online = 5;
            public PlayerData[] sample = new PlayerData[] {new PlayerData()};
            
        }

        private class PlayerData
        {
            public string name = "This is a test player";
            public string id = "4566e69f-c907-48ee-8d71-d7ba5aa00d20";
        }
    }

    //query response:
    /*
    {
    "version": {
        "name": "1.8.7",
        "protocol": 47
    },
    "players": {
        "max": 100,
        "online": 5,
        "sample": [
            {
                "name": "thinkofdeath",
                "id": "4566e69f-c907-48ee-8d71-d7ba5aa00d20"
            }
        ]
    },
    "description": {
        "text": "Hello world"
    },
    "favicon": "data:image/png;base64,<data>"
    }
    */
    
}